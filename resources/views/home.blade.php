@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

        </div>

        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-header">
                    <h5> My Skills
                        <a href="javascript:void(0)" class="btn btn-outline-secondary mb-2 float-right" id="create-new-skill">Add new skill</a>
                    </h5>
                </div>

                <div class="card-body">
                    @forelse ($userSkill as $item)
                        <span class="badge badge-secondary p-2">{{ $item }}</span>
                    @empty
                        No skill found!
                    @endforelse
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Listing of users list who are friends of logged user</div>

                <div class="card-body">
                    @forelse ($loggedRender as $item)
                        {!! $item !!}
                    @empty
                        No records found!
                    @endforelse
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Listing of those users which have the same skill only.</div>

                <div class="card-body">
                    @forelse ($commonSkillRender as $item)
                        {!! $item !!}
                    @empty
                        No records found!
                    @endforelse
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Who sends friend request.</div>

                <div class="card-body">
                    @forelse ($requestedRender as $item)
                        {!! $item !!}
                    @empty
                        No records found!
                    @endforelse
                </div>
            </div>
        </div>

    </div>
</div>

@include('Components.SkillModal')

@endsection

@push('script')
<script>
    $('img').on("error", function() {
        $(this).attr('src', '{{ url('/')."/profile/profile1.png" }}');
    });

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#create-new-skill').click(function () {
            $('#btn-save').val("create-skill");
            $('#skillForm').trigger("reset");
            $('#skillCrudModal').html("All Skills");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#skillForm').serialize(),
                url: "{{ route('user.skill.store') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);
                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    }

                    $('#btn-save').html('Save');
                }
            });
        });

        $('body').on('change', '.modal-attributes', function() {
            var currentValue = $(this).val();
            var spanTest = $(this).siblings('span').text;

            if( spanTest.length !== 0 && currentValue.length !== 0 ) {
                $(this).siblings('span').text('');
            }
        });

        $('body').on('click', '.btnCommonSkillUsers', function(e) {
            e.preventDefault();

            var actionType = $(this);
            var actionValue = $(this).text;
            var urlAction = $(this).attr('data-sendurl');

            $(this).html('Sending..');

            $.ajax({
                url: urlAction,
                type: "get",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    $('#btn-save').html(actionValue);
                }
            });
        });
    });
</script>
@endpush
