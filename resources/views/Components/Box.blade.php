<div class="jumbotron p-3">
    <div class="media">

        @if ($options['imageabled'])
            <img
                width="80" height="80"
                class="mr-3 d-block rounded-circle border border-secondary"
                src="{{ url('/') . "/profile/".$userData->userDetails->profile_pic}}"
                alt="{{ $userData->name }}"
            />
        @endif

        <div class="media-body">
            <h5 class="mt-0">{{ $userData->name }}</h5>

            @if ($options['creatabled'])
                <p><small><i>Created on {{ $userData->created_at->format('d M, Y') }}</i></small></p>
            @endif

            @if ($options['follwabled'])
                <p><small><i>Friend since {{ $userData->created_at->format('d M, Y') }}</i></small></p>
            @endif

            <button
                type="button"
                class="btn {{ $options['button']['class'] }} btn-block"
                data-sendurl="{{
                    route('user.relation.store', ['user' => $userData->id, 'type' => $options['type']])
                }}"
            >
                {{ $options['button']['text'] }}
            </button>
        </div>
      </div>
</div>
