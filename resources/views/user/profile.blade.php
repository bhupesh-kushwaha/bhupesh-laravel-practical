@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User Profile</div>

                <div class="card-body">

                    @if(session()->has('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Success!</strong> {{ session('status') }}

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <p><strong>Opps Something went wrong</strong></p>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success">{{session('success')}}</div>
                    @endif


                    <div class="row">
                        <div class="col-md-12">
                            <img
                                width="150" height="150"
                                class="mx-auto d-block rounded-circle border border-secondary"
                                src="{{ url('/')."/profile/".$user->userDetails->profile_pic }}"
                                alt="{{ $user->userDetails->username }}"
                            />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        <form
                            id="bs-validate-demo"
                            method="post"
                            action="{{ route('user.profile.post') }}"
                            class="{{ ($errors->any()) ? 'was-validated': ''}}"
                            enctype="multipart/form-data"
                            novalidate
                        >
                            @csrf
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_name">Full Name:</label>
                                    <input type="text" class="form-control" id="user_name" name="name" value="{{ $user->name }}" placeholder="Enter full Name" required />
                                        <div class="invalid-feedback">
                                            Please enter your full name!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_email">Your Email:</label>
                                        <input type="email" class="form-control" id="user_email" name="email" value="{{ $user->email }}" placeholder="Enter email" required />
                                        <div class="invalid-feedback">
                                            Please enter a valid email address!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_username">User Name:</label>
                                        <input type="text" class="form-control" id="user_username" name="username" value="{{ $user->userDetails->username }}" placeholder="Enter Last Name" readonly disabled />
                                        <div class="invalid-feedback">
                                            Please enter your username!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_mobile">Enter mobile number:</label>
                                        <input type="text" class="form-control" id="user_mobile" name="mobile" value="{{ $user->userDetails->mobile }}" placeholder="Enter mobile number" />
                                        <div class="invalid-feedback">
                                            Please enter your mobile number!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_gender">Select Year</label>
                                        <select class="form-control" id="user_gender" name="gender">
                                            <option value="">-- Select Gender --</option>
                                            <option {{ $user->userDetails->gender === 'male' ? 'selected' : ''  }} value="male">Male</option>
                                            <option {{ $user->userDetails->gender === 'female' ? 'selected' : ''  }} value="female">Fenale</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select your gender!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_profile_pic">Enter mobile number:</label>
                                        <input type="file" class="form-control" id="user_profile_pic" name="profile_pic" />
                                        <input type="hidden" class="form-control" id="user_profile_pic_old" name="profile_pic_old" value="{{ $user->userDetails->profile_pic }}" />
                                        <div class="invalid-feedback">
                                            Please select your profile_pic!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-dark">Update My Account</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $('img').on("error", function() {
        $(this).attr('src', '{{ url('/')."/profile/profile1.png" }}');
    });
</script>
@endpush
