<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/profile', 'HomeController@viewprofile')->name('user.profile.view');

Route::post('/user/edit/profile', 'HomeController@saveProfile')->name('user.profile.post');

Route::post('/user/skill/store', 'HomeController@saveUserSkill')->name('user.skill.store');

Route::get('/user/relation/{user}/{type}/store', 'HomeController@saveRelation')->name('user.relation.store');
