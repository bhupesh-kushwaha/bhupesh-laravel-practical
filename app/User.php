<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const USER_IS_LOGGED_IN = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The user detail that related to the user.
     */
    public function userDetails()
    {
        return $this->hasOne(UserDetail::class);
    }

    /**
     * The skill that belong to the user.
     */
    public function skills()
    {
        return $this->belongsToMany(Skill::class);
    }

    /**
     * Get the relation for the user.
     */
    public function relations()
    {
        return $this->hasMany(Relation::class, 'send_by', 'id');
    }

    /**
     * Get the accept relation for the user.
     */
    public function relationsAccepted()
    {
        return $this->relations()->where('status', Relation::FRIEND_REQUEST_ACCEPT);
    }

    /**
     * Get the requested relation for the user.
     */
    public function relationsRequested()
    {
        return $this->relations()->where('status', Relation::FRIEND_REQUEST_REQUEST);
    }
}
