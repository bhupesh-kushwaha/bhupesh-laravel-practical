<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    const FRIEND_REQUEST_REQUEST = 1;
    const FRIEND_REQUEST_ACCEPT = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'send_by', 'send_to', 'status',
    ];

    public static function getTableName()
    {
        return (new self())->getTable();
    }
}
