<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserProfileRequest;
use App\User;
use App\UserDetail;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\SkillRequest;
use App\Http\Traits\HelperTrait;
use App\Relation;
use App\Skill;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use HelperTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $skillRender = $this->renderSkillPage();

        $userSkill = User::where('id', Auth::user()->id)->with('skills')->get()->first()->skills->pluck('name');

        $commonSkillRender = $this->renderBoxPage('common-skill');
        $requestedRender = $this->renderBoxPage('requested');
        $loggedRender = $this->renderBoxPage('logged');

        return view('home', compact(
            'skillRender',
            'userSkill',
            'commonSkillRender',
            'requestedRender',
            'loggedRender'
        ));
    }

    /**
     * Show the user profile.
     *
     * @param Request $request
     * @return mix
     */
    public function viewprofile(Request $request)
    {
        $user = User::find(Auth::user()->id)->with('userDetails')->first();

        return view('user.profile', compact('user'));
    }

    /**
     * Show the user profile.
     *
     * @param UserProfileRequest $request
     * @return mix
     */
    public function saveProfile(UserProfileRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $request->mobile;
        $user->email = $request->email;
        $user->save;

        $userDetails = UserDetail::where('user_id',Auth::user()->id)->first();

        $profileImageName = $this->userFileUpload($request, $userDetails);

        $userName = explode('@', $request->email);

        $userDetails = UserDetail::where('user_id',Auth::user()->id)->first();
        $userDetails->username = Arr::first($userName);
        $userDetails->mobile = $request->mobile;
        $userDetails->gender = $request->gender;
        $userDetails->profile_pic = $profileImageName;
        $userDetails->save();

        return redirect()->route('user.profile.view')->with('status', 'User successfully updated');
    }


    /**
     * save user skill
     *
     * @param SkillRequest $request
     * @return json
     */
    public function saveUserSkill(SkillRequest $request)
    {
        $checkSkill = Skill::where('name', $request->name)->first();

        if( !$checkSkill ) {
            $skill = new Skill();
            $skill->name = $request->name;
            $skill->save();

            $skill->users()->attach(Auth::user()->id);
        }
        else {
            $checkSkill->users()->sync(Auth::user()->id, false);
        }

        return response()->json([ 'message' => 'New skill added' ]);
    }

    /**
     * save relation with logged in user
     *
     * @param User $request
     * @param strinf $type
     * @param Request $request
     * @return json
     */
    public function saveRelation(User $user, $type, Request $request){

        $checkRelationExits = $this->checkRelationExits($user->id, 'save');

        if( $type === 'request' ){
            $relation = Relation::where(['send_by' => Auth::user()->id,'send_to' => $user->id])->first();

            if( !$relation ){
                $relation = Relation::create([
                    'send_by' => Auth::user()->id,
                    'send_to' => $user->id,
                    'status' => Relation::FRIEND_REQUEST_REQUEST
                ]);
            }
        }
        else {
            $relation = Relation::where(['send_by' => $user->id,'send_to' => Auth::user()->id])->first();

            if( $relation ) {
                if( $type === 'accept' ) {
                    $relation->status = Relation::FRIEND_REQUEST_ACCEPT;
                    $relation->save();
                }

                if( $type === 'cancle' ) {
                    $relation->delete();
                }
            }
        }

        return response()->json([ 'status' => 'success',  'message' => $type . ' successfully saved', 'data' => $relation]);
    }
}
