<?php

namespace App\Http\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Relation;
use App\Skill;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

trait HelperTrait
{
    /**
     * Get all skill then render with view
     *
     * @return mix
     */
    public function renderSkillPage()
    {
        $skill = Skill::orderBy('id', 'desc')->pluck('name');

        return view('Components.DataList', compact('skill'))->render();
    }

    /**
     * Get all skill then render with view
     *
     * @param string $type
     * @return mix
     */
    public function renderBoxPage($type)
    {
        $options = array(
            'imageabled' => true,
            'creatabled' => false,
            'follwabled' => false,
            'type' => 'request',
            'button'=> array(
                'text' => "Sends Friend Request",
                'class' => "btnCommonSkillUsers btn-outline-secondary",
            )
        );

        $userList = [];

        if( $type === 'common-skill' ){
            $userList = User::whereIn('id', function($q) {
                $userId = Auth::user()->id;
                $q->select(
                    DB::raw("DISTINCT user_id FROM skill_user WHERE  skill_id in (SELECT skill_id FROM skill_user WHERE user_id = " . $userId. ") or user_id = " . $userId)
                );
            })->where('id','!=', Auth::user()->id)->get();

            $options['imageabled'] = true;
            $options['creatabled'] = true;
            $options['follwabled'] = false;
            $options['type'] = 'request';
            $options['button']['text'] = "Sends Friend Request";
            $options['button']['class'] = "btnCommonSkillUsers btn-outline-secondary";
        }

        if( $type === 'requested' || $type === 'logged' ) {
            $userIds = "";

            if( $type === 'requested' ) {
                $userIds = Relation::where([
                    'send_to'=> Auth::user()->id,
                    'status' => Relation::FRIEND_REQUEST_REQUEST
                ])->pluck('send_by');

                $userList = User::whereIn('id', $userIds)->get();

                $options['imageabled'] = true;
                $options['creatabled'] = false;
                $options['follwabled'] = false;
                $options['type'] = 'accept';
                $options['button']['text'] = "Accept Friend Request";
                $options['button']['class'] = "btnCommonSkillUsers btnRequestUsers btn-outline-success";
            }

            if( $type === 'logged' ) {
                $userIds = Relation::where([
                    'send_to'=> Auth::user()->id,
                    'status' => Relation::FRIEND_REQUEST_ACCEPT
                ])->pluck('send_by');

                $userList = User::whereIn('id', $userIds)->where('is_logged_in', User::USER_IS_LOGGED_IN)->get();

                $options['imageabled'] = true;
                $options['creatabled'] = true;
                $options['follwabled'] = true;
                $options['type'] = 'cancle';
                $options['button']['text'] = "Cancle Friend Request";
                $options['button']['class'] = "btnCommonSkillUsers btnRelationCancleUsers btn-outline-danger";
            }
        }


        $viewBox = array();

        $urlType = $type;
        foreach($userList as $userData) {
            //$options = $this->renderBoxOptions( $userData, $options );

            $viewBox[] = view('Components.Box', compact('userData', 'options'))->render();
        }

        return $viewBox;
    }

        /**
     * Get all skill then render with view
     *
     * @param object $userData
     * @param array $options
     * @return array
     */
    public function renderBoxOptions($userData, $options)
    {
        $buttonAction = $this->checkRelationExits( $userData->id, 'button-action' );

        if( !empty($buttonAction['request']) || !empty($buttonAction['accept']) ) {
            if( !empty($buttonAction['request']) ) {
                $options['imageabled'] = true;
                $options['creatabled'] = false;
                $options['follwabled'] = false;
                $options['button']['text'] = "Accept Friend Request";
                $options['button']['class'] = "btnRequestUsers btn-outline-success";
            }

            if( !empty($buttonAction['accept']) ) {
                $options['imageabled'] = true;
                $options['creatabled'] = true;
                $options['follwabled'] = true;
                $options['button']['text'] = "Cancle Friend Request";
                $options['button']['class'] = "btnRelationCancleUsers btn-outline-danger";
            }
        }
        else {
            $options['imageabled'] = true;
            $options['creatabled'] = true;
            $options['follwabled'] = false;
            $options['button']['text'] = "Sends Friend Request";
            $options['button']['class'] = "btnCommonSkillUsers btn-outline-secondary";
        }

        return $options;
    }

    /**
     * check whether a requested user id already exits or not
     *
     * @param integer $id
     * @return boolen
     */
    public function checkRelationExits($id, $action) {

        $currentUser = User::where('id', Auth::user()->id)->with([
            'relationsRequested' => function($q) use($id) {
                $q->where('send_to', $id);
            },
            'relationsAccepted' => function($q) use($id) {
                $q->where('send_to', $id);
            }
        ])->first()->toArray();

        $checkRelationRequested = $checkRelationAccept = null;

        if( !is_null($currentUser )) {
            if( !is_null($currentUser['relations_requested']) ) {
                $checkRelationRequested = Arr::first($currentUser['relations_requested']);
            }

            if( !is_null($currentUser['relations_accepted']) ) {
                $checkRelationAccept =  Arr::first($currentUser['relations_accepted']);
            }
        }

        if( $action === 'action' ){
            if( $checkRelationRequested || $checkRelationAccept ) {
                return true;
            }

            return false;
        }

        if( $action === 'button-action' ) {
            return array(
                'request' => $checkRelationRequested,
                'accept' => $checkRelationAccept
            );
        }

        if( $action === 'get-requested' ) {
            return $currentUser['relations_requested'];
        }

        return false;
    }

    /**
     * User profile upload and remove old profile image from path.
     *
     * @param oblect $request
     * @param oblect $userDetails
     * @return string
     */
    public function userFileUpload($request, $userDetails)
    {
        $profileImageName = $request->profile_pic_old;

        if ($request->hasFile('profile_pic')) {
            $image = $request->file('profile_pic');
            $profileImageName = rand(). time() . '.' . $image->getClientOriginalName();
            $image->move( public_path() . "/profile/", $profileImageName);

            $image_path = public_path()."/profile/".$userDetails->profile_pic;

            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        return $profileImageName;
    }
}
